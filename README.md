#"USBCheckIn: Preventing BadUSB Attacks by Forcing Human-Device Interaction"

This page provides a companion website/video for the paper 
published at the Privacy Security and Trust 2016 conference.

[video.mp4](https://bitbucket.org/sec3/usbcheckin/raw/71f064d8f0ce6e3412b6438dbc909226b7e29b43/video.mp4)